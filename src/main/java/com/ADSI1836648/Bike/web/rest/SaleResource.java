package com.ADSI1836648.Bike.web.rest;

import com.ADSI1836648.Bike.domain.Sale;
import com.ADSI1836648.Bike.repository.SaleRepository;
import com.ADSI1836648.Bike.service.ISaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class SaleResource {

    @Autowired
    ISaleService saleService;

    @PostMapping("/sales")
    public Sale create(@RequestBody Sale sale) {
        sale.setDateSale(LocalDateTime.now());
        return saleService.create(sale);
    }
@PostMapping("/sales/multiple")
    public Sale create(@RequestBody List<Sale> sale) {

    return saleService.createList(sale);
    }

    @GetMapping("/sales")
    public Iterable<Sale> read(){
        return saleService.read();
    }

    @GetMapping("/sales/{id}")
    public Optional<Sale> findById(@PathVariable int id){
        return saleService.findById(id);
    }

    @PutMapping("/sales")
    public Sale update(@RequestBody Sale sale){
        return saleService.update(sale);
    }
}
