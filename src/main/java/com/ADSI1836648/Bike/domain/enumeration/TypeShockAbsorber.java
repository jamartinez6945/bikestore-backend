package com.ADSI1836648.Bike.domain.enumeration;

public enum TypeShockAbsorber {
    RIGIDA,
    HARDTAIL,
    FULL_SUSPENSION
}
