package com.ADSI1836648.Bike.repository;

import com.ADSI1836648.Bike.domain.Client;
import com.ADSI1836648.Bike.service.dto.ClientWithSaleDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
@Repository
public interface ClientRepository extends JpaRepository<Client, Integer> {
    Optional<Client> findByDocumentNumber(String documentNumber);

    Iterable<Client> findAllByDocumentNumberEquals(String documentNumber);
    Client findByDocumentNumberEquals(String documentNumber);
    Iterable<Client> findByEmailEquals(String email);
    Iterable<Client> findByDocumentNumberContaining (String documentNumber);
    @Query(value = "SELECT c.id as clientId, c.name as name, c.email as email, c.documentNumber as documentNumber, c.phoneNumber as phoneNumber, b.model as model, b.serial as serial, b.price as price, b.id as bikeId, b.status as status, s.id as id, s.dateSale as dateSale FROM Client c INNER  JOIN Sale s ON s.client.id = c.id INNER JOIN Bike  b ON b.id = s.bike.id WHERE c.documentNumber =:document ")
    List<ClientWithSaleDTO> findClientByDocumentWithSale(@Param(value = "document") String document);

}
